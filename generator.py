import argparse
import json
from base64 import b64encode
from time import time

import yaml
from css_html_js_minify import css_minify, html_minify
from jinja2 import Template
from markdown2 import markdown

VERSION = '0.2 alpha'


def load_config(config_file_path: str) -> dict:
    config_format = config_file_path.rsplit('.', 1)[-1].lower()
    with open(config_file_path) as f:
        if config_format == 'json':
            return json.load(f)
        elif config_format in {'yaml', 'yml'}:
            return yaml.load(f)
        else:
            raise Exception('File type not supported, use: json, yaml or yml')


def load_template() -> Template:
    with open('templates/template.html') as f:
        return Template(f.read())


def load_description(description_file: str) -> str:
    description_format = description_file.rsplit('.', 1)[-1].lower()
    with open(description_file) as f:
        if description_format in {'html', 'htm'}:
            return f.read()
        elif description_format == 'md':
            return markdown(f.read())
        else:
            raise Exception('File type not supported, use: htm, html or md')


def load_stylesheet() -> str:
    with open('templates/stylesheet.css') as f:
        return f.read()


def get_argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser('Generate minecraft serve info page')
    parser.add_argument('config', help='config file, allowed format: json, yaml, yml')
    parser.add_argument('description', help='server description, allowed format: html, htm, md')
    parser.add_argument('header_image', help='header background image, allowed format: jpg, png and may be other...')
    parser.add_argument('--minify', help='When you use it, file should be smaller but almost no editable',
                        action='store_true', default=False)
    return parser


def load_header_image(image_path: str) -> (bytes, str):
    with open(image_path, 'rb') as f:
        return b64encode(f.read()).decode(), image_path.rsplit('.', 1)[-1]


if __name__ == '__main__':
    t0 = time()
    parser = get_argument_parser()
    args = parser.parse_args()

    config = load_config(args.config)
    stylesheet = load_stylesheet()
    if args.minify:
        stylesheet = css_minify(stylesheet)
    template = load_template()
    header_image, header_type = load_header_image(args.header_image)
    description = load_description(args.description)

    generated_html = template.render(
        stylesheet=stylesheet,
        description=description,
        info=config['data'],
        commands=config['commands'],
        links=config['links'],
        contacts=config['contacts'],
        rules=config['rules'],
        header_image=header_image,
        header_type=header_type,
        version=VERSION,
    )
    if args.minify:
        generated_html = html_minify(generated_html)

    with open('index.html', 'w') as f:
        f.write(generated_html)
    print('Generated in {} seconds'.format(time() - t0))
