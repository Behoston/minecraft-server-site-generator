# Minecraft server site generator
Prosty i użyteczny generator stron dla serwerów minecrat.

## Idea
Generator powstał na potrzeby wygodnego generowania strony informacyjnej dla serwera Knarf
z myślą o przyszłym zastosowaniu w hostingu Knarf. 

## Co daje generator?
Generator generuje **jednoplikową** stronę w `HTML` zawierającą:

1. Opis serwera
2. Zasady panujące na serwerze
3. Spis komend dostępnych na serwerze dla każdej z grup (takich jak `gracz`, `VIP`, `moderator`)
4. Listę linków do serwisów listujących serwery 
   (takich jak [minecraft-list](http://minecraft-list.pl), czy [gametracker](http://gametracker.com))
5. Kontakt z administracją

Dzięki temu, że generowany jest pojedynczy plik, stronę jest bardzo łatwo wrzucić na dowolny hosting stron, lub własny serwer www.

## Jak używać generatora
Generator napisany jest w Pythonie 3, wszystkie zależności zajdują się w pliku `requirements.txt`.

użycie: 
```bash
python3 generator.py ./example/config.json ./example/description.md ./example/header.jpeg
```
##### config
Plik zawierający:
* Dane serwera
* Dane kontaktowe
* Dostępne rangi
* Dostępne komendy

Plik może być w formacie `json` lub formacie `yaml`.
Przykładowe pliki konfiguracyjne w obu formatach znajdują się w folderze `example`. 
Oba formaty są równoważne, `yaml` (lub `yml`) jest często używany do konfiguracji pluginów w minecrafcie.

Polecamy jednak używać formatu `json`, ze względu na łatwiejsze wychwytywanie błędów.

#### description
Plik zawiera główną stronę serwera z opisem. Plik może być w formacie `html` lub `md`(Markdown).

Polecamy użyć formatu `Markdown`, jest on bardziej intuicyjny niż `html`.

#### logo
Plik z obrazkiem tła nagłówka. Może być w formacie `jpg` lub `png`. 
Być może inne formaty obrazków też zadziałają, sprawdź i daj nam znać :)

#### --minify
Jeśli chcesz zmniejszyć rozmiar strony (co przyśpieszy ładowanie) na końcu polecania dopisz `--minify`. 
Jednakże wtedy plik będzie bardzo ciężko edytować, więc jeśli chcesz poprawić wygenerowany html to nie używaj tej flagi. 

---
### Licencja
Możesz używać generatora jeśli:
* jest to użycie niekomercyjne
* zamieścisz informacje o autorze i link do kodu źródłowego (dzieje się to automatycznie, jeśli nie grzebałeś w kodzie)

Jeśli chcesz wykorzystać ten generator komercyjnie, skontaktuj się ze mną.

### Dewelopowanie/zaawansowane użycie
Jeśli masz ochotę to możesz dowolnie modyfikować kod, zmieniać plik styli, zmienić szablon lub coś do niego dodać.

Warunki są takie same jak w licencji: 
* nie możesz używać komercyjnie 
* musisz podać informacje o pierwotnym pochoodzeniu kodu 
* musisz udostępnić kod (jeśli bardzo się wstydzisz to nie musisz, dopóki ktoś o to nie poprosi)

### Plany na przyszłość

* Zrobić, żeby rangi były opcjonalne
* Dołożyć anglojęzyczny template
* Dodać obsługę typów kontaktów (takich jak email `mailto`) i może jakieś ikonki do tego
* Dodać lepsze kolorowanie rang (może deklarowanie kolorów)
* Dodać opcję zapisu do wielu plików zamiast do jednego

### Changelog

#### 0.2

* Dodana minifikacja